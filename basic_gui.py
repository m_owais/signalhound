import PySimpleGUI as sg
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, FigureCanvasAgg
from matplotlib.figure import Figure
import sys
import time
from bbdevice.bb_api import *
import numpy as np
import matplotlib.pyplot as plt
#import seaborn as sns; sns.set() # styling
import roslibpy
from datetime import datetime as dt
import csv
class bb60:
    def __init__(self,window):
        self.window = window
        self.start_freq_user = 950000000.0
        self.stop_freq_user = 1050000000.0
        self.span = self.stop_freq_user - self.start_freq_user
        self.center_span = self.start_freq_user + (self.span/2)
        self.reference_level = -30.0
        self.RBW = 10.0e3
        self.VBW = 10.0e3
        self.sweep_time = 0.001 
        self.device_status = False

    def open_device(self):
        # Open device
        self.handle = bb_open_device()["handle"]

        # Configure device
        #bb_configure_center_span(self.handle, 1.0e9, 100.0e6)
        bb_configure_center_span(self.handle, self.center_span, self.span)
        bb_configure_level(self.handle, self.reference_level, BB_AUTO_ATTEN)
        bb_configure_gain(self.handle, BB_AUTO_GAIN)
        bb_configure_sweep_coupling(self.handle, self.RBW, self.VBW, self.sweep_time, BB_RBW_SHAPE_FLATTOP, BB_NO_SPUR_REJECT)
        bb_configure_acquisition(self.handle, BB_MIN_AND_MAX, BB_LOG_SCALE)
        bb_configure_proc_units(self.handle, BB_POWER)

        # Initialize
        bb_initiate(self.handle, BB_SWEEPING, 0)
        query = bb_query_trace_info(self.handle)
        self.sweep_size = query["sweep_size"]
        self.start_freq = query["start_freq"]
        self.bin_size = query["bin_size"]

        #Update the status of device in Mulitline Box
        self.update_device_status()

    def sweep(self):
        # Get sweep
        sweep_max = bb_fetch_trace_32f(self.handle, self.sweep_size)["max"]
        timestamp = dt.now()
        freqs = [self.start_freq + i * self.bin_size for i in range(self.sweep_size)]
        
        #Find the peak amplitude here
        peak_power_index = np.argmax(sweep_max)

        return freqs,sweep_max,peak_power_index,timestamp

    def close_conn(self):
        # Device no longer needed, close it
        bb_close_device(self.handle)
        self.window['-STATUS-'].update("No Device Connected",text_color='Black')

    def update_device_status(self):
        self.window['-STATUS-'].update("Device Connected\n",text_color='Green')
        self.window['-STATUS-'].print('Sweep Size : {}'.format(self.sweep_size),text_color='Green')
        self.window['-STATUS-'].print('Start Freq : {}'.format(self.start_freq),text_color='Green')
        self.window['-STATUS-'].print('Bin Size : {}'.format(self.bin_size),text_color='Green')
        self.window['-STATUS-'].print('Reference Level : {}'.format(self.reference_level),text_color='Green')
        self.window['-STATUS-'].print('RBW : {}'.format(self.RBW),text_color='Green')
        self.window['-STATUS-'].print('VBW : {}'.format(self.VBW),text_color='Green')
        self.window['-STATUS-'].print('Sweep Time : {}'.format(self.sweep_time),text_color='Green')

def draw_figure(canvas, figure, loc=(0, 0)):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

def get_bb60_params(spec):
    freq_multiplier = {'Hz':1,'KHz':1e3,'MHz':1e6,'GHz':1e9}
    sg.theme('NeutralBlue')   # Add a touch of color
    layout = [  [sg.Text('Start Frequecy            '), sg.InputText(default_text = '950000000'),sg.Combo(['Hz','KHz','MHz','GHz'],default_value = 'Hz' ,key='-FREQ1-')],
                [sg.Text('Stop Frequecy             '), sg.InputText(default_text = '1050000000'),sg.Combo(['Hz','KHz','MHz','GHz'],default_value = 'Hz' ,key='-FREQ2-')],
                [sg.Text('Reference Level(dBm)     '), sg.InputText(default_text = '-30.0')],
                [sg.Text('RBW                       '),sg.InputText(default_text = '10'),sg.Combo(['Hz','KHz','MHz','GHz'],default_value = 'KHz' ,key='-RBW-')],
                [sg.Text('VBW                       '),sg.InputText(default_text = '10'),sg.Combo(['Hz','KHz','MHz','GHz'],default_value = 'KHz' ,key='-VBW-')],
                [sg.Text('Sweep Time (sec)          '),sg.InputText(default_text = '0.001')],
                [sg.Submit(), sg.Button('Cancel'),sg.Button('Use Default')]]
    # Create the Window
    window = sg.Window('Signal Hound BB60C Configurator', layout, finalize=True)
    #while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
        params = None
        window.close()
        return None
    if event == 'Use Default':
        spec.open_device()
        spec.device_status = True
        window.close()
        return None
    spec.start_freq_user = float(values[0]) * freq_multiplier[values['-FREQ1-']]
    spec.stop_freq_user = float(values[1]) * freq_multiplier[values['-FREQ2-']]
    spec.reference_level = float(values[2])
    spec.RBW = float(values[3]) * freq_multiplier[values['-RBW-']]
    spec.VBW = float(values[4]) * freq_multiplier[values['-VBW-']]
    spec.sweep_time = float(values[5])
    spec.open_device()
    spec.device_status = True
    window.close()
    return None


def main():
    #roslibpy server setup
    ROS_IP = "192.168.50.232"
    client = roslibpy.Ros(host=ROS_IP,port=9090)
    try:
        client.run()
        sg.popup_ok("Connected to ROS server:{}".format(ROS_IP))
        sh_publisher = roslibpy.Topic(client, '/spectrum_analyzer', 'std_msgs/String')
    except:
        sg.popup_ok("Failed to connect to ROS server")

    sg.theme('NeutralBlue')   # Add a touch of color
    # All the stuff inside your window.
    layout = [  [sg.Multiline(default_text='No Device Connected', no_scrollbar =True,size=(30, 20),key='-STATUS-'),sg.Canvas(size=(640, 480), key='-CANVAS-'),sg.Multiline(size=(40,10),no_scrollbar =True, key='-OUTPUT-')],
                [sg.Button('Open Device'),sg.Button('Close Device'),sg.Button('Start Sweep'),sg.Button('Stop Sweep'),sg.Button('Record Sweep')],
                #[sg.Text('ROS Connection status:'),sg.Multiline(size=(10,1),no_scrollbar=True,key='-ROS_STATUS-')],
                [sg.Text('Enter something on Row 2'), sg.InputText()],
                [sg.Button('Ok'), sg.Button('Cancel')] ]
    

    # Create the Window
    window = sg.Window('Signal Hound BB60C Configurator', layout, finalize=True)

    canvas_elem = window['-CANVAS-']
    canvas = canvas_elem.TKCanvas
    
    # draw the initial plot in the window
    fig = Figure()
    fig_agg = draw_figure(canvas, fig)

    ax = fig.add_subplot(111)
    ax.cla()
    ax.set_title("Spectrum Plot")
    ax.set_ylabel("Power (dB)")
    ax.set_xlabel("Frequency")
    ax.set_ylim([-200,0])
    fig_agg.draw()

    #Initiliazing the BB60C device
    spec = bb60(window)

    # Event Loop to process "events" and get the "values" of the inputs
    sweep_flag = False
    record_flag = False
    #device_open = False

    while True:
        event, values = window.read(timeout=0.1)
        # if(client.is_connected):
        #     window['-ROS_STATUS-'].update("Connected",text_color='Green')
        # else:
        #     window['-ROS_STATUS-'].update("Disconnected",text_color='RED')
        if (spec.device_status == True) and (sweep_flag == True):
            freqs,sweep,peak_power_index,timestamp = spec.sweep()
            ax.cla()
            ax.set_title("Spectrum Plot")
            ax.set_ylabel("Power (dB)")
            ax.set_xlabel("Frequency")
            ax.set_ylim([-200,0])
            ax.plot(freqs,sweep)
            fig_agg.draw()
            window['-OUTPUT-'].print("Time:{} \nPeak Amplitude:{} dbm\nPeak Frequency:{} MHz".format(timestamp,sweep[peak_power_index],freqs[peak_power_index]/1000000))
            window['-OUTPUT-'].print("----------")

            #Publish the data to ros
            if client.is_connected:
                sh_publisher.publish(roslibpy.Message({'data': 'Time:{},Peak Amp:{},Peak Freq:{}'.format(timestamp,sweep[peak_power_index],freqs[peak_power_index]/1000000)}))
            #Recording of power data
            if record_flag == True:
                sh_file.writerow([dt.timestamp(timestamp) , sweep[peak_power_index], freqs[peak_power_index]/1000000])
        if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
            #spec.close_conn()
            break
        if event == 'Open Device':
            if spec.device_status == True:
                sg.popup_error('Device already open!')
            else:
                user_input = get_bb60_params(spec)
        if event == 'Close Device':
            #Initiliazing the BB60C device
            if spec.device_status == True:
                spec.close_conn()
                spec.device_status = False
                ax.cla()
                fig_agg.draw()
                if record_flag == True:
                    sh_file_handle.close()

        if event == 'Start Sweep':
            sweep_flag = True
        if event == 'Stop Sweep':
            ax.cla()
            ax.set_title("Spectrum Plot")
            ax.set_ylabel("Power (dB)")
            ax.set_xlabel("Frequency")
            ax.set_ylim([-200,0])
            fig_agg.draw()
            sweep_flag = False
        if event == 'Record Sweep':
            if (sweep_flag == True) and (record_flag == False):
                file_name = './recordings/{}_spectrum_analyzer.csv'.format(dt.timestamp(dt.now()))
                sh_file_handle = open(file_name,mode='w+')
                sh_file = csv.writer(sh_file_handle,delimiter=',')
                temp = "#"
                for text in values['-STATUS-']:
                    if text != '\n':
                        temp = temp + text
                    else:
                        temp = temp + text + '#'
                sh_file_handle.write(temp+'\n')
                sh_file.writerow(['Timestamp','Peak_Amplitude (dBm)','Peak_Frequency (MHz)'])
                record_flag = True
            else:
                pass
        #print('You entered ', values[0])

    window.close()

if __name__ == '__main__':
    main()